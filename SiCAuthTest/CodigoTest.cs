﻿using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;

namespace SiCAuthTest
{
    public class CodigoTest
    {

        public CodigoTest()
        {

        }

        [Fact]
        public void TesteCodigoNaoDeveSerNuloOuVazio()
        {
            string codigo = "";
            string expected = "Códigos vazios não são válidos.";
            try
            {
                Codigo codigoclasse = new Codigo(codigo);
            }catch(Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TesteCodigoNaoDeveSerEspacosBrancos()
        {
            string codigo = " ";
            string expected = "Códigos vazios não são válidos.";
            try
            {
                Codigo codigoclasse = new Codigo(codigo);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TestCodigoNaoDeveTerPontuacao()
        {
            string codigo = ".";
            string expected = "O código não pode conter sinais de pontuação";
            try
            {
                Codigo codigoclasse = new Codigo(codigo);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TestCodigoNaoDeveTerCaracteresEspeciais()
        {
            string codigo = "<";
            string expected = "O código não pode conter caracteres especiais.";
            try
            {
                Codigo codigoclasse = new Codigo(codigo);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TestCodigoNaoDeveTerEspacosBrancos()
        {
            string codigo = "lapr ";
            string expected = "O código não pode conter espaços";
            try
            {
                Codigo codigoclasse = new Codigo(codigo);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TestCodigoValidado()
        {
            string codigo = "lapr8599";
            string esperado = "lapr8599";
           
            Codigo codigoclasse = new Codigo(codigo);

            Assert.Equal(codigoclasse.Conteudo,esperado);
            
        }

        [Fact]
        public void TestToDto()
        {
            string conteudo="lapr8599";
            CodigoDTO codigoDto = new CodigoDTO(conteudo);
            Codigo codigo = new Codigo(conteudo);
            CodigoDTO codigoResultado = codigo.ToDTO();
            Assert.Equal(codigoResultado,codigoDto);
        }

        
        [Fact]
        public void TestFromDto()
        {
            string conteudo="lapr8599";
            Codigo codigo=new Codigo(conteudo);
            CodigoDTO codigoDto= new CodigoDTO(conteudo);
            Codigo codigoEsperado=Codigo.FromDTO(codigoDto);
            Assert.Equal(codigo,codigoEsperado);
        }
        

    }
}
