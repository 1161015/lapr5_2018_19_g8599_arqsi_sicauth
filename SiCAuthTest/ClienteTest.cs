using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;
using TodoApi.Models;

namespace SiCAuthTest
{
    public class ClienteTest
    {

        public ClienteTest()
        {

        }

        [Fact]
        public void TestToDTOConversion()
        {

            UsernameDTO username = new UsernameDTO("GestorLapr");
            PasswordDTO password = new PasswordDTO("Lapr.8599");
            NomeDTO nome = new NomeDTO("Lapr");
            EmailDTO email = new EmailDTO("lapr8599@isep.ipp.pt");
            MoradaDTO morada = new MoradaDTO("Porto");

            string username1 = new string("GestorLapr");
            string password1 = new string("Lapr.8599");
            string nome1 = new string("Lapr");
            string email1 = new string("lapr8599@isep.ipp.pt");
            string morada1 = new string("Porto");

            Cliente cliente = new Cliente(username1, password1, nome1, email1, morada1, true, true);

            UtilizadorDTO expected = new UtilizadorDTO
            {
                Username = username,
                Password = password,
                Nome = nome,
                Email = email,
                Morada = morada,
                Cargo = cliente.GetType().Name,
                Termos = true,
                Tratamento = true,

            };

            UtilizadorDTO result = cliente.ToDTO();

            Assert.Equal(expected.Nome, result.Nome);
            Assert.Equal(expected.Morada, result.Morada);
            Assert.Equal(expected.Password, result.Password);
            Assert.Equal(expected.Username, result.Username);
            Assert.Equal(expected.Termos, result.Termos);
            Assert.Equal(expected.Tratamento, result.Tratamento);
        }


    }
}
