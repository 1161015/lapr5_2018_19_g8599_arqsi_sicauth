﻿using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;
using SicAuth.Services;
using TodoApi.Models;

namespace SiCAuthTest
{
    public class UtilizadorTeste
    {
        public UtilizadorTeste() { }

        [Fact]
        public void TestFromDtoCliente()
        {

            string username1 = new string("GestorLapr");
            string password1 = new string("Lapr.8599");
            string nome1 = new string("Lapr");
            string email1 = new string("lapr8599@isep.ipp.pt");
            string morada1 = new string("Porto");

            Cliente expected = new Cliente(username1, password1, nome1, email1, morada1, true, true);

            UsernameDTO username = new UsernameDTO("GestorLapr");
            PasswordDTO password = new PasswordDTO("Lapr.8599");
            NomeDTO nome = new NomeDTO("Lapr");
            EmailDTO email = new EmailDTO("lapr8599@isep.ipp.pt");
            MoradaDTO morada = new MoradaDTO("Porto");

            UtilizadorDTO dto = new UtilizadorDTO
            {
                Username = username,
                Password = password,
                Nome = nome,
                Email = email,
                Morada = morada,
                Cargo = expected.GetType().Name,
                Termos = true,
                Tratamento = true,

            };

            //Utilizador resultado = Utilizador.FromDTO(dto);

            Assert.Equal(expected, Utilizador.FromDTO(dto));
        }

        [Fact]
        public void TestFromDtoGestor()
        {

            string username1 = new string("GestorLapr");
            string password1 = new string("Lapr.8599");
            string nome1 = new string("Lapr");
            string email1 = new string("lapr8599@isep.ipp.pt");
            string morada1 = new string("Porto");
            
            GestorCatalogo expected = new GestorCatalogo(username1, password1, nome1, email1, true, true);

            UsernameDTO username = new UsernameDTO("GestorLapr");
            PasswordDTO password = new PasswordDTO("Lapr.8599");
            NomeDTO nome = new NomeDTO("Lapr");
            EmailDTO email = new EmailDTO("lapr8599@isep.ipp.pt");

            UtilizadorDTO dto = new UtilizadorDTO
            {
                Username = username,
                Password = password,
                Nome = nome,
                Email = email,
                Morada = null,
                Cargo = expected.GetType().Name,
                Termos = true,
                Tratamento = true,

            };

            Assert.Equal(expected, Utilizador.FromDTO(dto));
        }
    }
}
