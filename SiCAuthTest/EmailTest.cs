﻿using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;

namespace SiCAuthTest
{
    public class EmailTest
    {

        public EmailTest()
        {
        }

        [Fact]
        public void TesteValidarEmailSemArroba()
        {
            string emailString = "lapr8599";

            bool resultado = Email.ValidarEmail(emailString);

            Assert.False(resultado);

        }


        [Fact]
        public void TesteValidarEmailSemAntecedente()
        {
            string emailString = "@isep.ipp.pt";
            bool resultado = Email.ValidarEmail(emailString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidarEmailSemEndereco()
        {
            string emailString = "lapr8599@";
            bool resultado = Email.ValidarEmail(emailString);

            Assert.False(resultado);
        }

        [Fact]
        public void TestEmailValidado()
        {
            string email = "lapr8599@isep.ipp.pt";
            bool resultado = Email.ValidarEmail(email);

            Assert.True(resultado);

        }

        
        [Fact]
        public void TestToDto()
        {
            string conteudo="lapr8599@isep.ipp.pt";
            Email email=new Email(conteudo);
            EmailDTO emailDto= new EmailDTO(conteudo);
            Assert.Equal(emailDto,email.ToDTO());
        }
        

        
        [Fact]
        public void TestFromDto()
        {
            string conteudo="lapr8599@isep.ipp.pt";
            Email email=new Email(conteudo);
            EmailDTO emailDto= new EmailDTO(conteudo);
            Assert.Equal(email,Email.FromDTO(emailDto));
        }
    }
}
