﻿using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;

namespace SiCAuthTest
{
    public class NomeTest
    {

        public NomeTest()
        {
        }

        [Fact]
        public void TesteValidaNomeNuloOuVazia()
        {
            string nomeString = "";
            bool resultado = Nome.ValidarNome(nomeString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaNomeComEspacoBranco()
        {
            string nomeString = " ";
            bool resultado = Nome.ValidarNome(nomeString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaNomeComNumeros()
        {
            string nomeString = "Gestor1";
            bool resultado = Nome.ValidarNome(nomeString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaNomeComMenosDeTresCaracteres()
        {
            string nomeString = "Ge";
            bool resultado = Nome.ValidarNome(nomeString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteNomeValidado()
        {
            string nomeString = "Lapr";
            bool resultado = Nome.ValidarNome(nomeString);

            Assert.True(resultado);
        }

        [Fact]
        public void TestToDto()
        {
            string conteudo="nome";
            Nome nome=new Nome(conteudo);
            NomeDTO nomeDto= new NomeDTO(conteudo);
            Assert.Equal(nomeDto,nome.ToDTO());
        }
        

        
        [Fact]
        public void TestFromDto()
        {
            string conteudo="nome";
            Nome nome=new Nome(conteudo);
            NomeDTO nomeDto= new NomeDTO(conteudo);
            Assert.Equal(nome,Nome.FromDTO(nomeDto));
        }



    }
}
