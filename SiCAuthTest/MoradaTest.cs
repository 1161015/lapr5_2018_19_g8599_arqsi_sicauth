﻿using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;

namespace SiCAuthTest
{
    public class MoradaTest
    {
        public MoradaTest()
        {
        }

        [Fact]
        public void TesteValidaMoradaNulaOuVazia()
        {
            string moradaString = "";

            bool resultado = Morada.ValidarMorada(moradaString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaMoradaComEspacoEmBranco()
        {
            string moradaString = "  ";
            bool resultado = Morada.ValidarMorada(moradaString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaMoradaComMenosDeTresCaracteres()
        {
            string moradaString = "po";
            bool resultado = Morada.ValidarMorada(moradaString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteMoradaValidada()
        {
            string moradaString = "Porto";
            bool resultado = Morada.ValidarMorada(moradaString);

            Assert.True(resultado);
        }

        [Fact]
        public void TestToDto()
        {
            string conteudo="morada";
            MoradaDTO moradaDto = new MoradaDTO(conteudo);
            Morada morada = new Morada(conteudo);
            MoradaDTO moradaResultado = morada.ToDTO();
            Assert.Equal(moradaResultado,moradaDto);
        }

        
        [Fact]
        public void TestFromDto()
        {
            string conteudo="morada";
            Morada morada=new Morada(conteudo);
            MoradaDTO moradaDto= new MoradaDTO(conteudo);
            Morada moradaResultado=Morada.FromDTO(moradaDto);
            Assert.Equal(morada,moradaResultado);
        }

    }

}
