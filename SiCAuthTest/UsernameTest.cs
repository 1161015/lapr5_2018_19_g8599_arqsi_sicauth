﻿using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;
using SicAuth.Services;
using TodoApi.Models;


namespace SiCAuthTest
{
    public class UsernameTest
    {

        public UsernameTest() { }

        [Fact]
        public void TesteValidaUsernameNuloOuVazio()
        {
            string usernameString = "";
            string expected = "O username não pode estar vazio.";
            try
            {
                Username nome = new Username(usernameString);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TesteValidaUsernameComEspacosEmBranco()
        {
            string usernameString = "  ";
            string expected = "O username não pode estar vazio.";
            try
            {
                Username nome = new Username(usernameString);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TesteValidaUsernameComMenosDeSeisCaracteres()
        {
            string usernameString = "lapr";
            string expected = "O username tem que conter pelo menos 6 caracteres.";
            try
            {
                Username nome = new Username(usernameString);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TesteValidaUsernameComCaracteresEspeciais()
        {
            string usernameString = "lapr8599<";
            string expected = "O username não pode conter caracteres especiais.";
            try
            {
                Username nome = new Username(usernameString);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TesteValidaUsernameComPontuacao()
        {
            string usernameString = "lapr8599.";
            string expected = "O username não pode conter caracteres de pontuação.";
            try
            {
                Username nome = new Username(usernameString);
            }
            catch (Exception e)
            {
                Assert.Equal(e.Message, expected);
            }
        }

        [Fact]
        public void TestUsernameValidado()
        {
            string username = "Laprlapr8599";
            string esperado = "Laprlapr8599";

            Username usernameClasse = new Username(username);

            Assert.Equal(usernameClasse.Conteudo, esperado);

        }

        [Fact]
        public void TestToDto()
        {
            string conteudo="Laprlapr8599";
            Username username=new Username(conteudo);
            UsernameDTO usernameDto= new UsernameDTO(conteudo);
            Assert.Equal(usernameDto,username.ToDTO());
        }
        

        
        [Fact]
        public void TestFromDto()
        {
            string conteudo="Laprlapr8599";
            Username username=new Username(conteudo);
            UsernameDTO usernameDto= new UsernameDTO(conteudo);
            Assert.Equal(username,Username.FromDTO(usernameDto));
        }


    }
}
