﻿using System;
using Xunit;

using System.Collections.Generic;
using System.Diagnostics;
using SicAuth.ViewModels;
using SicAuth.Models;
using SicAuth.Services;
using TodoApi.Models;

namespace SiCAuthTest
{
    public class PasswordTest
    {

        public PasswordTest() { }

        [Fact]
        public void TesteValidaPasswordNuloOuVazia()
        {

            string passwordString = "";

            bool resultado = Password.ValidarPassword(passwordString);

            Assert.False(resultado);
        
        }

        [Fact]
        public void TesteValidaPasswordComEspaco()
        {

            string passwordString = " ";

            bool resultado = Password.ValidarPassword(passwordString);

            Assert.False(resultado);

        }

        [Fact]
        public void TesteValidaPasswordComMenosDeOitoCaracteres()
        {
            string passwordString = "Lapr.8";

            bool resultado = Password.ValidarPassword(passwordString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaPasswordSemDigitos()
        {
            string passwordString = "Laprlapr.lapr";

            bool resultado = Password.ValidarPassword(passwordString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaPasswordSemMaiusculas()
        {
            string passwordString = "laprlapr.lapr";

            bool resultado = Password.ValidarPassword(passwordString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaPasswordSemMinusculas()
        {
            string passwordString = "LAPRLAPRLAPR.";

            bool resultado = Password.ValidarPassword(passwordString);

            Assert.False(resultado);
        }

        [Fact]
        public void TesteValidaPasswordSemPontuacao()
        {
            string passwordString = "LaprLaprlapr";

            bool resultado = Password.ValidarPassword(passwordString);

            Assert.False(resultado);
        }

        [Fact]
        public void TestPasswordValidada()
        {
            string password = "Laprlapr.8599";
            string esperado = "Laprlapr.8599";

            Password passwordClasse = new Password(password);

            Assert.Equal(passwordClasse.Conteudo, esperado);

        }

        [Fact]
        public void TestToDto()
        {
            string conteudo="Laprlapr.8599";
            Password password=new Password(conteudo);
            PasswordDTO passwordDto= new PasswordDTO(conteudo);
            Assert.Equal(passwordDto,password.ToDTO());
        }
        

        
        [Fact]
        public void TestFromDto()
        {
            string conteudo="Laprlapr.8599";
            Password password=new Password(conteudo);
            PasswordDTO passwordDto= new PasswordDTO(conteudo);
            Assert.Equal(password,Password.FromDTO(passwordDto));
        }

    }
}
