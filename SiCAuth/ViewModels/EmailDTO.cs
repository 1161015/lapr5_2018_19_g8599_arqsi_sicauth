﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SicAuth.ViewModels
{
    public class EmailDTO
    {
        public string Email { get; set; }

        public EmailDTO(string email)
        {
            this.Email = email;
        }

        public override bool Equals(object obj)
        {
            var item = obj as EmailDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Email.Equals(item.Email))
            {
                return false;
            }
            return true;
        }

         public override int GetHashCode()
        {
            return Email.GetHashCode();
        }
    }
}
