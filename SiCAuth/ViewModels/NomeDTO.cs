﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SicAuth.ViewModels
{
    public class NomeDTO
    {
        public string Nome { get; set; }

        public NomeDTO(string nome)
        {
            this.Nome = nome;
        }

        public override bool Equals(object obj)
        {
            var item = obj as NomeDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Nome.Equals(item.Nome))
            {
                return false;
            }
            return true;
        }

         public override int GetHashCode()
        {
            return Nome.GetHashCode();
        }
    }
}
