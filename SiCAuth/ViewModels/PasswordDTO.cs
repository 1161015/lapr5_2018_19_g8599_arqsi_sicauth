﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SicAuth.ViewModels
{
    public class PasswordDTO
    {
        public string Password { get; set; }

        public PasswordDTO(string password)
        {
            this.Password = password;
        }

        public override bool Equals(object obj)
        {
            var item = obj as PasswordDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Password.Equals(item.Password))
            {
                return false;
            }
            return true;
        }

         public override int GetHashCode()
        {
            return Password.GetHashCode();
        }
    }
}
