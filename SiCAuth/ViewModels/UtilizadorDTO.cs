﻿namespace SicAuth.ViewModels
{
    public class UtilizadorDTO
    {
        public long? Id { get; set; }
        public NomeDTO Nome { get; set; }
        public EmailDTO Email { get; set; }
        public PasswordDTO Password { get; set; }
        public UsernameDTO Username { get; set; }
        public MoradaDTO Morada { get; set; }
        public CodigoDTO Codigo { get; set; }
        public string Cargo { get; set; }
        public string Token { get; set; }
        public bool Termos { get; set; }
        public bool Tratamento { get; set; }

    }



   
}
