﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SicAuth.ViewModels
{
    public class UsernameDTO
    {
        public string Username { get; set; }

        public UsernameDTO(string username)
        {
            this.Username = username;
        }

        public override bool Equals(object obj)
        {
            var item = obj as UsernameDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Username.Equals(item.Username))
            {
                return false;
            }
            return true;
        }

         public override int GetHashCode()
        {
            return Username.GetHashCode();
        }
    }
}
