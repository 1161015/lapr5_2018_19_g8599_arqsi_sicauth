﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SicAuth.ViewModels
{
    public class MoradaDTO
    {
        public string Morada { get; set; }

        public MoradaDTO(string morada)
        {
            this.Morada = morada;
        }

        public override bool Equals(object obj)
        {
            var item = obj as MoradaDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Morada.Equals(item.Morada))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return Morada.GetHashCode();
        }
    }
}
