﻿using System;

namespace SicAuth.ViewModels
{
    public class CodigoDTO
    {
        public String Codigo { get; set; }

        public CodigoDTO(string codigo)
        {
            Codigo = codigo;
        }

         public override bool Equals(object obj)
        {
            var item = obj as CodigoDTO;

            if (item == null)
            {
                return false;
            }

            if (!this.Codigo.Equals(item.Codigo))
            {
                return false;
            }
            return true;
        }

         public override int GetHashCode()
        {
            return Codigo.GetHashCode();
        }

        
    }
}
