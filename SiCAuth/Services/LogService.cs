﻿using SicAuth.Utils;
using SiCAuth.Utils;
using System.Threading.Tasks;
using TodoApi.Models;

namespace SicAuth.Services
{
    public class LogService
    {
        private readonly SicAuthContext contexto;

        public LogService(SicAuthContext contexto)
        {
            this.contexto = contexto;
        }

        public async Task InserirLog(long utilizadorId, string descricaoEvento, long? idDbEvento, long importancia)
        {
            var utilizadorIdInserir = AesEncrypt.Encrypt(utilizadorId.ToString(), Program.CHAVE);
            string idDbEventoInserir = null;
            if (idDbEvento != null)
            {
                idDbEventoInserir = AesEncrypt.Encrypt(idDbEvento.ToString(), Program.CHAVE);
            }
            var logAdicionar = new Log(utilizadorIdInserir, descricaoEvento, idDbEventoInserir, importancia);
            contexto.Logs.Add(logAdicionar);
            await contexto.SaveChangesAsync();
        }
    }
}
