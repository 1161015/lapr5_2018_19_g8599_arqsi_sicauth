﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SicAuth.Models;
using SicAuth.ViewModels;
using SiCAuth.Utils;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TodoApi.Models;

namespace SicAuth.Services
{
    public class UtilizadorService
    {

        private static readonly string EMAIL_EMPRESA = "lapr5.g8599@gmail.com";
        private static readonly string PASSWORD_EMAIL_EMPRESA = "Facil123";
        public SicAuthContext Contexto { get; set; }
        public object AesEncryptamajig { get; private set; }

        public UtilizadorService(SicAuthContext contexto)
        {
            Contexto = contexto;
        }

        public long IdUtilizador(Username username)
        {
            return Contexto.Utilizadores.Where(u => u.Username.Equals(username)).FirstOrDefault().Id;
        }

        public bool ExisteEmail(Utilizador utilizador)
        {
            foreach (var utilizadorAtual in Contexto.Utilizadores)
            {
                var emailAtual = AesEncrypt.Decrypt(utilizadorAtual.Email.Conteudo, Program.CHAVE);
                if (utilizador.Email.Conteudo.Equals(emailAtual, StringComparison.InvariantCultureIgnoreCase)) return true;
            }
            return false;
        }

        public bool ExisteUsername(Utilizador utilizador)
        {
            return Contexto.Utilizadores.Where(u => u.Username.Equals(utilizador.Username)).FirstOrDefault() != null;
        }

        public void EncriptarPassword(Utilizador utilizador)
        {
            var passwordEncryptada = BCrypt.Net.BCrypt.HashPassword(utilizador.Password.Conteudo);
            utilizador.Password.Conteudo = passwordEncryptada;
        }

        public bool AtualizarInformacao(long idUtilizador, UtilizadorDTO utilizadorDTO)
        {
            var utilizadorDB = Contexto.Utilizadores.Find(idUtilizador);
            if (utilizadorDB == null) return false;
            if (utilizadorDTO.Password != null) // o user pode não querer alterar a password
            {
                if (!Password.ValidarPassword(utilizadorDTO.Password.Password)) return false;
                utilizadorDB.Password.Conteudo = BCrypt.Net.BCrypt.HashPassword(utilizadorDTO.Password.Password);
            }
            if (!Nome.ValidarNome(utilizadorDTO.Nome.Nome) || !Morada.ValidarMorada(utilizadorDTO.Morada.Morada)) return false;

            utilizadorDB.Nome.Conteudo = utilizadorDTO.Nome.Nome; // nome alterado
            utilizadorDB.Tratamento = utilizadorDTO.Tratamento; // tratamento dos dados RGPD
            if (utilizadorDB is Cliente)
            {
                var castUtilizador = (Cliente)utilizadorDB;
                castUtilizador.Morada.Conteudo = utilizadorDTO.Morada.Morada;
                utilizadorDB = castUtilizador;
            }
            EncriptarDadosPessoais(utilizadorDB);
            Contexto.Entry(utilizadorDB).State = EntityState.Modified;
            return true;
        }

        public void EncriptarEmail(Utilizador utilizador)
        {
            utilizador.Email.Conteudo = AesEncrypt.Encrypt(utilizador.Email.Conteudo, Program.CHAVE);
        }

        public void EncriptarDadosPessoais(Utilizador utilizador)
        {
            utilizador.Nome.Conteudo = AesEncrypt.Encrypt(utilizador.Nome.Conteudo, Program.CHAVE);
            
            if (utilizador is Cliente)
            {
                Cliente utilizadorCast = (Cliente)utilizador;
                utilizadorCast.Morada.Conteudo = AesEncrypt.Encrypt(utilizadorCast.Morada.Conteudo, Program.CHAVE);
                utilizador = utilizadorCast;
            }
        }

        public bool RevalidarUtilizador(UtilizadorDTO utilizador)
        {
            var utilizadorDB = Contexto.Utilizadores.Find(utilizador.Id);
            if (utilizadorDB == null) return false;
            return BCrypt.Net.BCrypt.Verify(utilizador.Password.Password, utilizadorDB.Password.Conteudo);
        }

        public async Task RecuperarPassword(UsernameDTO usernameDTO, EmailDTO emailDTO)
        {
            var username = Username.FromDTO(usernameDTO);
            var email = Email.FromDTO(emailDTO);
            Utilizador utilizadorBD = null;
            foreach(var utilizadorAtual in Contexto.Utilizadores)
            {
                var emailAtual = AesEncrypt.Decrypt(utilizadorAtual.Email.Conteudo, Program.CHAVE);
                if(utilizadorAtual.Username.Equals(username) && emailAtual.Equals(email.Conteudo, StringComparison.InvariantCultureIgnoreCase))
                {
                    utilizadorBD = utilizadorAtual;
                    break;
                }
            }
            if (utilizadorBD == null) return; // caso o username não exista
            var novaPassword = GerarPassword();
            string textoEmail = "Ora boas " + AesEncrypt.Decrypt(utilizadorBD.Nome.Conteudo,Program.CHAVE) + " parece-me que esqueceu-se novamente da sua palavra passe, felizmente não há problema que a gente não resolva.\n" +
               "Toma-lá esta nova password e vê se não te esqueces mais!\n" +
               "Nova password:\n" + novaPassword;
            var emailGuardado = new Email(utilizadorBD.Email.Conteudo);
            EnviarEmail("Nova palavra-passe Sic-Cliente", textoEmail, utilizadorBD.Email);
            utilizadorBD.Password.Conteudo = novaPassword;
            EncriptarPassword(utilizadorBD);
            utilizadorBD.Email = emailGuardado;
            var logService = new LogService(Contexto);
            await logService.InserirLog(utilizadorBD.Id, "Foi enviada uma nova password, a pedido do utilizador", null, 4);
            Contexto.Entry(utilizadorBD).State = EntityState.Modified;
            Contexto.SaveChanges();
        }

        private void EnviarEmail(string assunto, string mensagem, Email email)
        {
            var emailDecrypt = AesEncrypt.Decrypt(email.Conteudo, Program.CHAVE);
            email.Conteudo = emailDecrypt;
            MailMessage emailEnviar = new MailMessage(EMAIL_EMPRESA, email.Conteudo);
            emailEnviar.Subject = assunto;
            emailEnviar.Body = mensagem;
            SmtpClient clienteSmtp = new SmtpClient();
            clienteSmtp.Host = "smtp.gmail.com";
            clienteSmtp.Port = 587;
            clienteSmtp.UseDefaultCredentials = false;
            clienteSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            clienteSmtp.EnableSsl = true;
            clienteSmtp.Credentials = new NetworkCredential(EMAIL_EMPRESA, PASSWORD_EMAIL_EMPRESA);
            clienteSmtp.Timeout = 30000;
            clienteSmtp.Send(emailEnviar);
        }

        private string GerarPassword()
        {
            string passwordGerada = "";
            char[] maisculas = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            char[] minusculas = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            char[] algarismos = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            char[] pontuacoes = { '.', ',', '!', '?', '@', '€', '£' };
            int j;
            Random gerador = new Random();
            while (passwordGerada.Length < 10) //garantir que tem 10 caracteres
            {
                j = gerador.Next(4); // gera de 0 a 3 
                switch (j)
                {
                    case 0: // maisculas
                        int maiusculaIndex = gerador.Next(maisculas.Length);
                        passwordGerada += maisculas[maiusculaIndex];
                        break;
                    case 1: // minusculas
                        int minusculaIndex = gerador.Next(minusculas.Length);
                        passwordGerada += minusculas[minusculaIndex];
                        break;
                    case 2: // algarismos
                        int algorismoIndex = gerador.Next(algarismos.Length);
                        passwordGerada += algarismos[algorismoIndex];
                        break;
                    case 3:
                        int pontuacaoIndex = gerador.Next(pontuacoes.Length);
                        passwordGerada += pontuacoes[pontuacaoIndex];
                        break;
                }
            }
            if (!TemNumero(passwordGerada))
            {
                int algorismoIndex = gerador.Next(algarismos.Length);
                passwordGerada += algarismos[algorismoIndex];
            }
            if (!TemMinuscula(passwordGerada))
            {
                int minusculaIndex = gerador.Next(minusculas.Length);
                passwordGerada += minusculas[minusculaIndex];
            }
            if (!TemMaiuscula(passwordGerada))
            {
                int maiusculaIndex = gerador.Next(maisculas.Length);
                passwordGerada += maisculas[maiusculaIndex];
            }
            if (!TemPontuacao(passwordGerada))
            {
                int pontuacaoIndex = gerador.Next(pontuacoes.Length);
                passwordGerada += pontuacoes[pontuacaoIndex];
            }
            return passwordGerada;
        }

        private bool TemNumero(string password)
        {
            return password.Any(char.IsDigit);
        }

        private bool TemMaiuscula(string password)
        {
            return password.Any(char.IsUpper);
        }

        private bool TemMinuscula(string password)
        {
            return password.Any(char.IsLower);
        }

        private bool TemPontuacao(string password)
        {
            return password.Any(char.IsPunctuation);
        }


        public async Task<UtilizadorDTO> Autenticar(UsernameDTO usernameDTO, PasswordDTO passwordDTO)
        {
            var username = Username.FromDTO(usernameDTO);
            var utilizadorBD = Contexto.Utilizadores.Where(u => u.Username.Equals(username)).FirstOrDefault();
            if (utilizadorBD == null) return null; // caso o username não exista
            var hashPassword = utilizadorBD.Password.Conteudo;
            if (!BCrypt.Net.BCrypt.Verify(passwordDTO.Password, hashPassword))
            {
                await AtualizarTentativasFalhadas(utilizadorBD);
                return null; // caso a password esteja errada
            }
            await ResetarTentativas(utilizadorBD);
            var utilizadorDTO = new UtilizadorDTO();
            utilizadorDTO = utilizadorBD.ToDTO();
            return utilizadorDTO;
        }

        private async Task AtualizarTentativasFalhadas(Utilizador utilizador)
        {

            utilizador.TentativasFalhadas++;
            if (utilizador.TentativasFalhadas % 5 == 0 && utilizador.TentativasFalhadas > 0)
            {
                LogService logService = new LogService(Contexto);
                await logService.InserirLog(utilizador.Id, "Este utilizador teve 5 tentativas seguidas de login mal sucedidas (password incorreta).", null, 6);
                var guardarEmail = new Email(utilizador.Email.Conteudo);
                EnviarEmail("Tentativas de acesso mal sucedidas", "Ocorreram 5 tentativas mal sucedidas de login na sua conta, por favor verifique que apenas você tem acesso à conta, é também recomendado alterar a sua password.\n Caso não se lembre da sua palavra passe, utilize a função de recuperar password disponibilizada no website.", utilizador.Email);
                utilizador.Email = guardarEmail;
            }
            Contexto.Entry(utilizador).State = EntityState.Modified;
            await Contexto.SaveChangesAsync();

        }

        private async Task ResetarTentativas(Utilizador utilizador)
        {
            utilizador.TentativasFalhadas = 0;
            Contexto.Entry(utilizador).State = EntityState.Modified;
            await Contexto.SaveChangesAsync();
        }

        public void GerarCodigoEmail(UtilizadorDTO utilizadorDTO)
        {
            string codigoGerado = GerarCodigoEmail(); // o código gerado tem um tempo de vida

            var username = Username.FromDTO(utilizadorDTO.Username);
            var utilizadorDB = Contexto.Utilizadores.Where(u => u.Username.Equals(username)).FirstOrDefault();
            utilizadorDB.Codigo.Conteudo = codigoGerado;
            Contexto.Entry(utilizadorDB).State = EntityState.Modified;
            Contexto.SaveChanges();
            EnviarEmail("Código Autenticação", "Como último passo, por favor insira este código:\n" + codigoGerado + "", utilizadorDB.Email);
            // GerarTimerApagarCodigo(utilizadorDTO,60000);
        }

        public void GerarTokenJwt(UtilizadorDTO utilizador)
        {
            // Agora passar à criação do token JWT
            var utilizadorDB = Contexto.Utilizadores.Where(u => u.Username.Equals(Username.FromDTO(utilizador.Username))).FirstOrDefault();
            var tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = null;
            if (utilizadorDB is Cliente)
            {
                key = Encoding.ASCII.GetBytes("6cQ4vG4aeP6ATcJulsoiQBOUawU5VciM");
            }
            else if (utilizadorDB is GestorCatalogo)
            {
                key = Encoding.ASCII.GetBytes("gUkzDCHvVg5Dop041GNd7ZLnUprm2jJB");
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, utilizadorDB.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            utilizador.Token = tokenHandler.WriteToken(token);
            utilizador.Cargo = utilizadorDB.GetType().Name;
            utilizador.Id = utilizadorDB.Id;
        }

        private string GerarCodigoEmail()
        {
            string codigo = "";
            char[] maisculas = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            char[] minusculas = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            char[] algarismos = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            Random geradorRandoms = new Random();
            int numeroGerado;
            while (codigo.Length < 6)
            {
                numeroGerado = geradorRandoms.Next(3); // 0 ou 1 ou 2
                switch (numeroGerado)
                {
                    case 0: // maiusculas
                        int indiceMaiusculas = geradorRandoms.Next(maisculas.Length);
                        codigo += maisculas[indiceMaiusculas];
                        break;
                    case 1: // minusculas
                        int indiceMinusculas = geradorRandoms.Next(minusculas.Length);
                        codigo += minusculas[indiceMinusculas];
                        break;
                    case 2: // minusculas
                        int indiceAlgarismos = geradorRandoms.Next(algarismos.Length);
                        codigo += algarismos[indiceAlgarismos];
                        break;
                    default:
                        break;
                }
            }
            return codigo;
        }

        public bool VerificarCodigoEmail(Username username, Codigo codigo)
        {
            var utilizadorDB = Contexto.Utilizadores.Where(u => u.Username.Equals(username)).FirstOrDefault();
            if (utilizadorDB == null) return false;
            return utilizadorDB.Codigo.Equals(codigo);
        }

        public async void GerarTimerApagarCodigo(UtilizadorDTO utilizadorDTO, int delay)
        {
            await Task.Delay(delay); // espera sem bloquear, como se fosse uma thread separada
            string codigoGerado = GerarCodigoEmail();
            var username = Username.FromDTO(utilizadorDTO.Username);
            var utilizadorDB = Contexto.Utilizadores.Where(u => u.Username.Equals(username)).FirstOrDefault();
            if (utilizadorDB == null) return; // caso o user tenha apagado a conta no intervalo de tempo
            utilizadorDB.Codigo.Conteudo = null;
            Contexto.Entry(utilizadorDB).State = EntityState.Modified;
            Contexto.SaveChanges();
        }

    }
}
