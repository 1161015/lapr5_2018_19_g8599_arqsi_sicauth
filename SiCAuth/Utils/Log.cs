﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SicAuth.Utils
{
    public class Log
    {
        public long Id { get; set; } // gerado automaticamente pela DB
        public string UtilizadorId { get; set; } // id do utilizador que gerou o evento
        public string DescricaoEvento { get; set; } // se foi um post, delete, put etc
        public string IdDbEvento { get; set; } // id do novo recurso criado (ou atualizado ou apagado), pode ser null no caso de um get 
        public long Importancia { get; set; } // importância do evento, consoante algum critério
        public DateTime DataEvento { get; set; } 

        private Log()
        {

        }

        public Log(string utilizadorId, string descricaoEvento, string idDbEvento, long importancia)
        {
            UtilizadorId = utilizadorId;
            DescricaoEvento = descricaoEvento;
            IdDbEvento = idDbEvento;
            Importancia = importancia;
            DataEvento = DateTime.Now;
        }
    }
}
