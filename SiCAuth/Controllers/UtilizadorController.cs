using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SicAuth.Models;
using SicAuth.Services;
using SicAuth.ViewModels;
using SiCAuth.Utils;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace SicAuth.Controllers
{
    [Authorize]
    [Route("api/utilizador")]
    [ApiController]
    public class UtilizadorController : ControllerBase
    {
        private readonly SicAuthContext _context;

        public LogService ServicoLog;

        public UtilizadorService ServicoUtilizador;

        public UtilizadorController(SicAuthContext context)
        {
            _context = context;
            ServicoUtilizador = new UtilizadorService(context);
            ServicoLog = new LogService(context);
        }

        // GET: api/Utilizador/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUtilizador([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var utilizador = await _context.Utilizadores.FindAsync(id);

            if (utilizador == null)
            {
                return NotFound();
            }
            var retorno = utilizador.ToDTO();
            retorno.Morada.Morada = AesEncrypt.Decrypt(retorno.Morada.Morada, Program.CHAVE);
            retorno.Nome.Nome = AesEncrypt.Decrypt(retorno.Nome.Nome, Program.CHAVE);
            retorno.Password = null;
            retorno.Username = null;
            retorno.Email = null;
            retorno.Cargo = null;
            return Ok(retorno);
        }

        [AllowAnonymous]
        [HttpPost("recuperar")]
        public async Task<IActionResult> RecuperarPassword([FromBody] UtilizadorDTO utilizadorDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await ServicoUtilizador.RecuperarPassword(utilizadorDTO.Username, utilizadorDTO.Email);
            return NoContent();
        }

        [AllowAnonymous]
        [HttpPost("codigo")]
        public async Task<IActionResult> VerificarCodigoEmail([FromBody] UtilizadorDTO utilizadorDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool isValid = ServicoUtilizador.VerificarCodigoEmail(Username.FromDTO(utilizadorDTO.Username), Codigo.FromDTO(utilizadorDTO.Codigo));
            if (isValid)
            {
                ServicoUtilizador.GerarTokenJwt(utilizadorDTO);
                long idUtilizador = ServicoUtilizador.IdUtilizador(Username.FromDTO(utilizadorDTO.Username));
                var enderecoUtilizador = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress;
                var enderecoEncriptado = AesEncrypt.Encrypt(enderecoUtilizador.ToString(), Program.CHAVE);
                await ServicoLog.InserirLog(idUtilizador, "Utilizador logado com sucesso, apartir do endere�o de rede (encriptado): " + enderecoEncriptado, null, 2);
                return Ok(utilizadorDTO);
            }
            return new ContentResult() { StatusCode = 406, Content = "C�digo inserido incorreto" };
        }
        // PUT: api/Utilizador/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUtilizador([FromRoute] long id, [FromBody] UtilizadorDTO utilizador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != utilizador.Id)
            {
                await ServicoLog.InserirLog((long)utilizador.Id, "Viola��o de dados: tentativa de altera��o de dados pessoais de outro utilizador.", id, 8);
                string resposta = " { message: 'N�o tem permiss�es para alterar dados de outro utilizador'}";
                return new ContentResult() { Content = resposta, ContentType = "application/json", StatusCode = 403 };
            }


            if (!ServicoUtilizador.AtualizarInformacao(id, utilizador))
            {
                return new ContentResult() { StatusCode = 404, ContentType = "Atualiza��o dos dados falhou, verifique os par�metros inseridos" };
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtilizadorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            await ServicoLog.InserirLog(id, "Dados deste utilizador atualizados!", id, 5);

            return NoContent();
        }

        [HttpPost("revalidarutilizador")]
        public IActionResult RevalidarUtilizador([FromBody] UtilizadorDTO utilizadorDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool isValid = ServicoUtilizador.RevalidarUtilizador(utilizadorDTO);
            if (isValid)
            {
                utilizadorDTO.Password = null;
                return Ok(utilizadorDTO);
            }
            return new ContentResult() { StatusCode = 406, Content = "Password incorreta." };
        }

        // POST: api/Utilizador
        [AllowAnonymous] // n�o � requerido autentica��o para criar uma conta
        [HttpPost]
        public async Task<IActionResult> PostUtilizador([FromBody] UtilizadorDTO utilizadorDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var utilizador = Utilizador.FromDTO(utilizadorDTO);
            if (utilizadorDTO.Morada != null)
            {
                if (!Morada.ValidarMorada(utilizadorDTO.Morada.Morada)) return new ContentResult() { Content = "Campos n�o respeitam os crit�rios.", StatusCode = 406, ContentType = "application/json" };
            }
            if (!Password.ValidarPassword(utilizadorDTO.Password.Password)
                || !Nome.ValidarNome(utilizadorDTO.Nome.Nome)
                || !Email.ValidarEmail(utilizadorDTO.Email.Email))
            {
                return new ContentResult() { Content = "Campos n�o respeitam os crit�rios.", StatusCode = 406, ContentType = "application/json" };
            }
            utilizador.Codigo = new Codigo();
            if (ServicoUtilizador.ExisteEmail(utilizador))
            {
                return new ContentResult() { Content = "J� existe um utilizador com esse email", StatusCode = 409, ContentType = "application/json" };
            }
            if (ServicoUtilizador.ExisteUsername(utilizador))
            {
                return new ContentResult() { Content = "J� existe um utilizador com esse username", StatusCode = 409, ContentType = "application/json" };
            }
            ServicoUtilizador.EncriptarPassword(utilizador);
            ServicoUtilizador.EncriptarEmail(utilizador);
            ServicoUtilizador.EncriptarDadosPessoais(utilizador);
            utilizador.TentativasFalhadas = 0;
            _context.Utilizadores.Add(utilizador);
            await _context.SaveChangesAsync();
            var enderecoIp = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress;
            var enderecoEncriptado = AesEncrypt.Encrypt(enderecoIp.ToString(), Program.CHAVE);
            var tipoUtilizador = utilizador.GetType().Name;
            await ServicoLog.InserirLog(utilizador.Id, "Foi criada uma conta do tipo " + tipoUtilizador + " com endere�o de rede: " + enderecoEncriptado, utilizador.Id, 3);

            utilizador.Password = null; // n�o enviar a password
            return CreatedAtAction("GetUtilizador", new { id = utilizador.Id }, utilizador);
        }

        [AllowAnonymous]
        [HttpPost("autenticacao")]
        public async Task<IActionResult> Autenticar([FromBody] UtilizadorDTO utilizadorDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var utilizadorDTORetorno = await ServicoUtilizador.Autenticar(utilizadorDTO.Username, utilizadorDTO.Password);
            if (utilizadorDTORetorno == null)
            {
                return new ContentResult() { Content = "Username ou senha inv�lida.", StatusCode = 403, ContentType = "application/json" };
            }
            // nunca enviar a password
            utilizadorDTORetorno.Password = null;
            utilizadorDTORetorno.Morada = null;
            ServicoUtilizador.GerarCodigoEmail(utilizadorDTO);
            return Ok(utilizadorDTORetorno);
        }
        // DELETE: api/Utilizador/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUtilizador([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var utilizador = await _context.Utilizadores.FindAsync(id);
            if (utilizador == null)
            {
                return NotFound();
            }

            _context.Utilizadores.Remove(utilizador);
            await _context.SaveChangesAsync();
            await ServicoLog.InserirLog(id, "Utilizador decidiu apagar a conta.", id, 6);
            return Ok(utilizador);
        }

        private bool UtilizadorExists(long id)
        {
            return _context.Utilizadores.Any(e => e.Id == id);
        }
    }
}