﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using SiCAuth.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace SicAuth
{
    public class Program
    {
        public static string CHAVE = "V6R2BmHsJJKMhNFRQQLV";
        public static void Main(string[] args)
        {
           // LerChave();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static void LerChave()
        {
            var data = new Dictionary<string, string>();
            foreach (var row in File.ReadAllLines("SicAuth.properties"))
            {
                data.Add(row.Split('=')[0], string.Join("=", row.Split('=').Skip(1).ToArray()));
            }
            CHAVE = data["chave_aes"];

        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
