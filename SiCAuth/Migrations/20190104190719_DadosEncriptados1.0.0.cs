﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SiCAuth.Migrations
{
    public partial class DadosEncriptados100 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UtilizadorId = table.Column<string>(nullable: true),
                    DescricaoEvento = table.Column<string>(nullable: true),
                    IdDbEvento = table.Column<string>(nullable: true),
                    Importancia = table.Column<long>(nullable: false),
                    DataEvento = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Utilizadores",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Username_Conteudo = table.Column<string>(nullable: true),
                    Password_Conteudo = table.Column<string>(type: "varchar(60)", nullable: true),
                    Nome_Conteudo = table.Column<string>(nullable: true),
                    Email_Conteudo = table.Column<string>(nullable: true),
                    Codigo_Conteudo = table.Column<string>(nullable: true),
                    Termos = table.Column<bool>(nullable: false),
                    Tratamento = table.Column<bool>(nullable: false),
                    TentativasFalhadas = table.Column<long>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    Morada_Conteudo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilizadores", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "Utilizadores");
        }
    }
}
