﻿using Microsoft.EntityFrameworkCore;
using SicAuth.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SicAuth.Models
{
    [Owned]
    public class Morada
    {
        public string Conteudo { get; set; }

        public Morada(string conteudo)
        {
           // ValidarMorada(conteudo); 
            this.Conteudo = conteudo;
        }

        public MoradaDTO ToDTO()
        {
            return new MoradaDTO(this.Conteudo);
        }

        public static Morada FromDTO(MoradaDTO moradaDTO)
        {
            return new Morada(moradaDTO.Morada);
        }

        public static bool ValidarMorada(string morada)
        {
            if(string.IsNullOrEmpty(morada) || string.IsNullOrWhiteSpace(morada))
            {
                return false;
            }
            if(morada.Length < 3)
            {
                return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !obj.GetType().Equals(GetType()) ) return false; // se for null, ou de outra classe
            if (this == obj) return true; // se apontarem para o mesmo endereço de memória
            Morada outraMorada = (Morada)obj;
            return Conteudo.Equals(outraMorada.Conteudo, StringComparison.InvariantCultureIgnoreCase);
        }
        public override int GetHashCode()
        {
            return Conteudo.GetHashCode();
        }


    }
}
