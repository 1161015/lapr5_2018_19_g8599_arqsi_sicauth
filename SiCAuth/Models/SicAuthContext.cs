using Microsoft.EntityFrameworkCore;
using SicAuth.Models;
using SicAuth.Utils;

namespace TodoApi.Models
{
    public class SicAuthContext : DbContext
    {
        public SicAuthContext(DbContextOptions<SicAuthContext> options)
             : base(options)
        {
        }

        public SicAuthContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // atualmente o EFC s� suporta  Table Per Hierarchy (TPH), onde � utilizada uma tabela
            // para representar todas as entidades
            modelBuilder.Entity<Cliente>().HasBaseType<Utilizador>();
            modelBuilder.Entity<GestorCatalogo>().HasBaseType<Utilizador>();
            //modelBuilder.Entity<Cliente>().HasIndex(c => c.Email).IsUnique();
            //modelBuilder.Entity<GestorCatalogo>().HasIndex(c => c.Email).IsUnique();
            //modelBuilder.Entity<Utilizador>().HasIndex(u => u.Nome).IsUnique();
        }


        public DbSet<Utilizador> Utilizadores { get; set; }
        public DbSet<Log> Logs { get; set; }
        //  public DbSet<GestorCatalogo> GestoresCatalogos { get; set; }

    }
}