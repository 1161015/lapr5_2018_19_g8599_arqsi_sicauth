﻿using Microsoft.EntityFrameworkCore;
using SicAuth.ViewModels;
using System;
using System.Linq;

namespace SicAuth.Models
{
    [Owned]
    public class Username
    {

        public string Conteudo { get; set; }

        public Username(string conteudo)
        {
            ValidarUsername(conteudo);
            Conteudo = conteudo;
        }

        private void ValidarUsername(string username)
        {

            if (String.IsNullOrEmpty(username) || String.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException("O username não pode estar vazio.");
            }
            if (username.Length < 6)
            {
                throw new ArgumentException("O username tem que conter pelo menos 6 caracteres.");
            }
            if (username.Any(char.IsSymbol))
            {
                throw new ArgumentException("O username não pode conter caracteres especiais.");
            }
            if (username.Any(char.IsPunctuation))
            {
                throw new ArgumentException("O username não pode conter caracteres de pontuação.");
            }
        }

        public UsernameDTO ToDTO()
        {
            return new UsernameDTO(Conteudo);
        }

        public static Username FromDTO(UsernameDTO dto)
        {
            return new Username(dto.Username);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !obj.GetType().Equals(GetType())) return false;
            if (this == obj) return true;
            Username username = (Username)obj;
            return Conteudo.Equals(username.Conteudo, StringComparison.InvariantCultureIgnoreCase);
        }

        public override int GetHashCode()
        {
            return Conteudo.GetHashCode();
        }
    }
}
