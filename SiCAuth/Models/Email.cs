﻿using Microsoft.EntityFrameworkCore;
using SicAuth.ViewModels;
using System;
using System.Net.Mail;

namespace SicAuth.Models
{
    [Owned]
    public class Email
    {
        public string Conteudo { get; set; }

        public Email(string conteudo)
        {
           // ValidarEmail(conteudo);
            Conteudo = conteudo;
        }

        public static bool ValidarEmail(string email)
        {
            try
            {
                MailAddress ma = new MailAddress(email);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public EmailDTO ToDTO()
        {
            return new EmailDTO(Conteudo);
        }

        public static Email FromDTO(EmailDTO dto)
        {
            return new Email(dto.Email);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !obj.GetType().Equals(GetType()) ) return false; // se for null, ou de outra classe
            if (this == obj) return true; // se apontarem para o mesmo endereço de memória
            Email outroEmail = (Email)obj;
            return Conteudo.Equals(outroEmail.Conteudo, StringComparison.InvariantCultureIgnoreCase);
        }
        public override int GetHashCode()
        {
            return Conteudo.GetHashCode();
        }
    }
}
