﻿using SicAuth.ViewModels;
using TodoApi.Models;

namespace SicAuth.Models
{
    public class Cliente : Utilizador
    {

        public Morada Morada { get; set; }
        public Cliente(string username, string password, string nome, string email, string morada, bool termos, bool tratamento)
            : base(username, password, nome, email,termos,tratamento)
        {
            Morada = new Morada(morada);
        }

        protected Cliente()
        {

        }

        public override UtilizadorDTO ToDTO()
        {
            return new UtilizadorDTO
            {
                Username = Username.ToDTO(),
                Password = Password.ToDTO(),
                Nome = Nome.ToDTO(),
                Email = Email.ToDTO(),
                Morada = Morada.ToDTO(),
                Cargo = this.GetType().Name,
                Termos = Termos,
                Tratamento = Tratamento,

            };
        }
    }
}
