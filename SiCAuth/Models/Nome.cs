﻿using Microsoft.EntityFrameworkCore;
using SicAuth.ViewModels;
using System;
using System.Linq;

namespace SicAuth.Models
{
    [Owned] // Value object
    public class Nome
    {
        public string Conteudo { get; set; }

        public Nome(string conteudo)
        {
            //ValidarNome(conteudo);
            Conteudo = conteudo;
        }

        public static bool ValidarNome(string conteudo)
        {
            if (String.IsNullOrEmpty(conteudo) || String.IsNullOrWhiteSpace(conteudo))
            {
                return false;
            }
            if (conteudo.Any(char.IsDigit))
            {
                return false;
            }
            if (conteudo.Length < 3)
            {
                return false;
            }
            return true;
        }

        public NomeDTO ToDTO()
        {
            return new NomeDTO(Conteudo);
        }

        public static Nome FromDTO(NomeDTO nomeDTO)
        {
            return new Nome(nomeDTO.Nome);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !obj.GetType().Equals(GetType()) ) return false; // se for null, ou de outra classe
            if (this == obj) return true; // se apontarem para o mesmo endereço de memória
            Nome outroNome = (Nome)obj;
            return Conteudo.Equals(outroNome.Conteudo, StringComparison.InvariantCultureIgnoreCase);
        }
        public override int GetHashCode()
        {
            return Conteudo.GetHashCode();
        }
    }
}
