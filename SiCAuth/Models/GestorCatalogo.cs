﻿using SicAuth.ViewModels;
using TodoApi.Models;

namespace SicAuth.Models
{
    public class GestorCatalogo : Utilizador
    {
        public GestorCatalogo(string username, string password, string nome, string email, bool termos, bool tratamento)
            : base(username, password, nome, email, termos, tratamento) { }

        protected GestorCatalogo()
        {

        }

        public override UtilizadorDTO ToDTO()
        {
            return new UtilizadorDTO
            {
                Username = Username.ToDTO(),
                Password = Password.ToDTO(),
                Nome = Nome.ToDTO(),
                Email = Email.ToDTO(),
                Cargo = this.GetType().Name,
                Termos = Termos,
                Tratamento = Tratamento
            };
        }

    }
}
