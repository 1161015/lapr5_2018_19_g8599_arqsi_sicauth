﻿using Microsoft.EntityFrameworkCore;
using SicAuth.ViewModels;
using System;
using System.Linq;

namespace SicAuth.Models
{
    [Owned]
    public class Codigo
    {
        public string Conteudo { get; set; }

        public Codigo(string conteudo)
        {
            ValidarCodigo(conteudo);
            Conteudo = conteudo;
        }

        public Codigo()
        {
            Conteudo = null;
        }

        public CodigoDTO ToDTO()
        {
            return new CodigoDTO(Conteudo);
        }

        public static Codigo FromDTO(CodigoDTO codigoDTO)
        {
            return new Codigo(codigoDTO.Codigo);
        }

        private void ValidarCodigo(string codigo)
        {
            if (string.IsNullOrEmpty(codigo) || string.IsNullOrWhiteSpace(codigo))
            {
                throw new ArgumentException("Códigos vazios não são válidos.");
            }
            if (codigo.Any(char.IsPunctuation))
            {
                throw new ArgumentException("O código não pode conter sinais de pontuação");
            }
            if (codigo.Any(char.IsSymbol))
            {
                throw new ArgumentException("O código não pode conter caracteres especiais.");
            }
            if (codigo.Any(char.IsWhiteSpace))
            {
                throw new ArgumentException("O código não pode conter espaços");
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !obj.GetType().Equals(GetType())) return false;

            if (obj == this) return true;

            var codigo = (Codigo)obj;

            return Conteudo.Equals(codigo.Conteudo);
        }

        public override int GetHashCode()
        {
            return Conteudo.GetHashCode();
        }
    }
}
