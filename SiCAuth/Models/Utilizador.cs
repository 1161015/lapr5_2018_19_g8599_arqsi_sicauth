using SicAuth.Models;
using SicAuth.ViewModels;
using System;

namespace TodoApi.Models
{
    public abstract class Utilizador
    {
        public long Id { get; set; }
        public Username Username { get; set; }
        public Password Password { get; set; }
        public Nome Nome { get; set; }
        public Email Email { get; set; }
        public Codigo Codigo { get; set; }
        public bool Termos { get; set; }
        public bool Tratamento { get; set; }
        public long TentativasFalhadas { get; set; }
        public Utilizador(string username, string password, string nome, string email, bool termos, bool tratamento)
        {
            Username = new Username(username);
            Password = new Password(password);
            Nome = new Nome(nome);
            Email = new Email(email);
            Codigo = null;
            if (!termos) throw new ArgumentException("Os termos e servi�os t�m de ser aceites.");
            Termos = termos;
            Tratamento = tratamento;
        }

        public abstract UtilizadorDTO ToDTO();

        public static Utilizador FromDTO(UtilizadorDTO dto) // talvez seria adequado o padr�o Factory (temos pena n�o h� tempo)
        {
            Utilizador retorno = null;
            if (dto.Morada != null) // se tiver o cart�o de cr�dito preenchido ent�o trata-se de um cliente 
            {
                retorno = new Cliente(dto.Username.Username, dto.Password.Password, dto.Nome.Nome, dto.Email.Email, dto.Morada.Morada,dto.Termos,dto.Tratamento);
            }
            else // trata-se de um gestor de cat�logos
            {
                retorno = new GestorCatalogo(dto.Username.Username, dto.Password.Password, dto.Nome.Nome, dto.Email.Email,dto.Termos,dto.Tratamento);
            }
            return retorno;
        }

        protected Utilizador()
        {

        }

        public override bool Equals(object obj)
        {
            if (obj == null || !obj.GetType().Equals(GetType())) return false;
            if (this == obj) return true;
            Utilizador utilizador = (Utilizador)obj;

            if (utilizador.Codigo != null || Codigo != null)
                return Nome.Equals(utilizador.Nome) && Username.Equals(utilizador.Username) && Password.Equals(utilizador.Password)
                    && Email.Equals(utilizador.Email) && Codigo.Equals(utilizador.Codigo);
            else
                return Nome.Equals(utilizador.Nome) && Username.Equals(utilizador.Username) && Password.Equals(utilizador.Password)
                && Email.Equals(utilizador.Email);
        }

        public override int GetHashCode()
        {
            
            return Username.GetHashCode() + Password.GetHashCode() + Nome.GetHashCode() 
                + Email.GetHashCode() + Codigo.GetHashCode();
        }



    }
}