﻿using Microsoft.EntityFrameworkCore;
using SicAuth.ViewModels;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace SicAuth.Models
{
    [Owned]
    public class Password
    {
        [Column(TypeName = "varchar(60)")]
        public string Conteudo { get; set; }

        public Password(String conteudo)
        {
            // ValidarPassword(conteudo);
            Conteudo = conteudo;
        }

        public static bool ValidarPassword(string conteudo)
        {

            if (String.IsNullOrEmpty(conteudo) || String.IsNullOrWhiteSpace(conteudo) || conteudo.Length < 8 || !conteudo.Any(char.IsDigit)
               || !conteudo.Any(char.IsUpper) || !conteudo.Any(char.IsLower) || !conteudo.Any(char.IsPunctuation)) return false;
            return true; 

        }

        public PasswordDTO ToDTO()
        {
            return new PasswordDTO(Conteudo);
        }

        public static Password FromDTO(PasswordDTO passwordDTO)
        {
            return new Password(passwordDTO.Password);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !obj.GetType().Equals(GetType()) ) return false; // se for null, ou de outra classe
            if (this == obj) return true; // se apontarem para o mesmo endereço de memória
            Password outraPassword = (Password)obj;
            return Conteudo.Equals(outraPassword.Conteudo, StringComparison.InvariantCultureIgnoreCase);
        }
        public override int GetHashCode()
        {
            return Conteudo.GetHashCode();
        }
    }
}
